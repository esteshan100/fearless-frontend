// Waits for the web page's content to fully load before executing the script.
window.addEventListener('DOMContentLoaded', async () => {

    // Define the endpoint URL to fetch locations.
    const url = 'http://localhost:8000/api/locations/';

    // Send a GET request to retrieve locations data.
    const response = await fetch(url);

    // Check if the server responded with a success status (HTTP 200 OK).
    if (response.ok) {
        // Parse the response JSON into a JavaScript object.
        const data = await response.json();

        // Retrieve the 'select' HTML element for locations using its ID.
        const selectTag = document.getElementById('location');

        // Loop over each location from the received data.
        for (let location of data.locations) {
            // Create a new dropdown 'option' element for the location.
            const option = document.createElement('option');

            // Use the location's ID as the value for the option.
            option.value = location.id;

            // Use the location's name as the display text for the option.
            option.innerHTML = location.name;

            // Add the newly created option to the 'select' dropdown element.
            selectTag.appendChild(option);
        }
    }

    // Retrieve the form HTML element using its ID.
    const formTag = document.getElementById('create-conference-form');

    // Add an event listener to handle the form submission.
    formTag.addEventListener('submit', async event => {
        // Prevent the default form submission behavior.
        event.preventDefault();

        // Collect form data and serialize it into a JSON string.
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        // Define the endpoint URL to post a new conference.
        const locationUrl = 'http://localhost:8000/api/conferences/';

        // Configure the fetch request parameters for the POST request.
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        // Send a POST request with the serialized form data.
        const conferenceResponse = await fetch(locationUrl, fetchConfig);

        // If the POST request was successful, reset the form and log the newly created conference.
        if (conferenceResponse.ok) {
            formTag.reset();
            const newConference = await conferenceResponse.json();
            console.log(newConference);
        }
    });
});
