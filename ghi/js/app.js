/**
 * Creates an HTML card element for displaying conference information.
 *
 * @param {string} name - The title of the conference.
 * @param {string} description - A brief description of the conference.
 * @param {string} pictureUrl - URL for the conference picture.
 * @returns {string} The HTML string representing the card.
 */
function createCard(name, description, pictureUrl) {
  return `
    <div class="card">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <p class="card-text">${description}</p>
      </div>
    </div>
  `;
}

// Waits for the web page's content to fully load before executing the script.
window.addEventListener('DOMContentLoaded', async () => {

    // Define the endpoint URL to fetch conference data.
    const url = 'http://localhost:8000/api/conferences/';

    try {
      // Attempt to retrieve the conference data.
      const response = await fetch(url);

      // Check if the server responded with a non-success status.
      if (!response.ok) {
        // Handle any non-successful server responses.
        console.error('Bad response');
      } else {
        // Parse the response JSON into a JavaScript object.
        const data = await response.json();

        // Loop over each conference item in the data.
        for (let conference of data.conferences) {
            // Construct the detail URL for each conference.
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);

            // Check if the detail fetch was successful.
            if (detailResponse.ok) {
                // Parse the detailed data into a JavaScript object.
                const details = await detailResponse.json();

                // Extract the required data for the card.
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;

                // Create an HTML card using the extracted details.
                const html = createCard(title, description, pictureUrl);

                // Append the newly created card to the column.
                const column = document.querySelector('.col');
                column.innerHTML += html;
            }
        }
      }
    } catch (e) {
      // Handle any errors encountered during the fetch operation.
      console.error('Error was raised', e);
    }
});
