// This script initializes once the DOM content is fully loaded.
window.addEventListener('DOMContentLoaded', async () => {
    // Define the endpoint URL for retrieving states.
    const url = 'http://localhost:8000/api/states/';

    // Fetch data from the endpoint.
    const response = await fetch(url);

    // Check if the response is successful (HTTP status 200 OK).
    if (response.ok) {
        // Parse the JSON response into a JavaScript object.
        const data = await response.json();

        // Get the 'select' element for states by its ID.
        const selectTag = document.getElementById('state');

        // Populate the 'select' element with states.
        for (let state of data.states) {
            // Create a new option element for the dropdown.
            const option = document.createElement('option');

            // Use the state's abbreviation as the value for the option.
            option.value = state.abbreviation;

            // Use the state's name as the display text for the option.
            option.innerHTML = state.name;

            // Add the newly created option to the 'select' element.
            selectTag.appendChild(option);
        }
    }

    // Get the form element by its ID.
    const formTag = document.getElementById('create-location-form');

    // Add an event listener to handle the form submission.
    formTag.addEventListener('submit', async event => {
        // Prevent the default form submission behavior.
        event.preventDefault();

        // Collect and serialize form data into JSON format.
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        // Define the endpoint URL for posting a new location.
        const locationUrl = 'http://localhost:8000/api/locations/';

        // Configure fetch request parameters.
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        // Send the POST request with the form data.
        const locationResponse = await fetch(locationUrl, fetchConfig);

        // If the post was successful, reset the form and log the newly created location.
        if (locationResponse.ok) {
            formTag.reset();
            const newLocation = await locationResponse.json();
            console.log(newLocation);
        }
    });
});
